"""initial

Revision ID: c55a69026a25
Revises: 
Create Date: 2022-09-08 15:42:15.116228

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'c55a69026a25'
down_revision = None
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.pre_upgrade()
    op.create_table('example_kv_entry',
    sa.Column('key', sa.String(), nullable=False),
    sa.Column('e_tag', sa.Integer(), nullable=False),
    sa.Column('value_1', sa.String(), nullable=True),
    sa.Column('value_2', sa.String(), nullable=False),
    sa.PrimaryKeyConstraint('e_tag')
    )
    with op.batch_alter_table('example_kv_entry', schema=None) as batch_op:
        batch_op.create_index(batch_op.f('ix_example_kv_entry_e_tag'), ['e_tag'], unique=False)
        batch_op.create_index(batch_op.f('ix_example_kv_entry_key'), ['key'], unique=True)

    op.post_upgrade()
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.pre_downgrade()
    with op.batch_alter_table('example_kv_entry', schema=None) as batch_op:
        batch_op.drop_index(batch_op.f('ix_example_kv_entry_key'))
        batch_op.drop_index(batch_op.f('ix_example_kv_entry_e_tag'))

    op.drop_table('example_kv_entry')
    op.post_downgrade()
    # ### end Alembic commands ###

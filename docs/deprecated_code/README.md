# Deprecated Code

This contains deprecated code, or code we pushed out of the library, for reference.
The code is still working, we just do not see it fit for the library itself. If you 
require it, you can copy it here to your project and still use it.
"""key_error

Revision ID: a7ea100a784f
Revises: 812a3e5b8517
Create Date: 2024-05-23 10:28:25.382815

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'a7ea100a784f'
down_revision = '812a3e5b8517'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.pre_upgrade()
    # changed to invalid table name to force a migration error
    with op.batch_alter_table('test_access_model', schema=None) as batch_op:
        batch_op.drop_index('ix_test_access_model_regex')
        batch_op.drop_column('regex')
        batch_op.drop_column('regex')  # double delete forces 'KEYERROR'

    op.post_upgrade()
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.pre_downgrade()
    with op.batch_alter_table('test_access_model', schema=None) as batch_op:
        batch_op.add_column(sa.Column('regex', sa.VARCHAR(), nullable=True))
        batch_op.create_index('ix_test_access_model_regex', ['regex'], unique=False)

    op.post_downgrade()
    # ### end Alembic commands ###
